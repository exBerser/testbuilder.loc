
require('./bootstrap');

window.Vue = require('vue');


import App from './components/App'
import router from './router/index'
import store from './store/index'

const app = new Vue({
    el: '#app',
    template: '<app></app>',
    components: {App},
    router,
    store,
});