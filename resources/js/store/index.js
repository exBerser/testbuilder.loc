import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        auth: false,
        token: localStorage.getItem('token') || '',
        registerRole: 1,
    },

    getters: {
        isAuth: state => state.auth,
        getToken: state => state.token
    },

    mutations: {
        setAuth(state, payload) {
            state.auth = payload;
        },

        setToken(state, payload) {
            state.token = payload;
        },

        setRegisterRole(state, payload) {
            state.registerRole = payload;
        }
    },

    actions: {
        login(ctx, payload) {
            return axios.post('/api/auth/login', payload)
                .then(res => {
                    if (!res.data.error) {
                        localStorage.setItem('token', res.data.access_token);
                        ctx.commit('setToken', res.data.access_token);
                        ctx.commit('setAuth', true);
                    }

                    return res;
                });
        },

        register(ctx, payload) {
            return axios.post('/api/auth/register', payload)
                .then(res => {
                    localStorage.setItem('token', res.data.access_token);
                    ctx.commit('setToken', res.data.access_token);
                    ctx.commit('setAuth', true);

                    return res;
                });
        },

        logout(ctx) {
            return axios.get('/api/auth/logout')
                .then(res => {
                    localStorage.setItem('token', '');
                    ctx.commit('setAuth', res.data.auth);
                    ctx.commit('setToken', '');
                })
        },

        checkAuth(ctx) {
            axios.post('/api/auth/check', {},  {headers: { 'Authorization': 'Bearer ' + this.getters.getToken} })
                .then(res => {
                    if (res.data.auth) {
                        ctx.commit('setAuth', true);
                    }
                })
        }
    }
});