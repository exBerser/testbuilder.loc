import Home from '../pages/Home.vue';
import Login from '../pages/Login.vue';
import Registration from '../pages/Registration.vue';
import Cards from '../pages/Cards.vue';

export default [
    {
        path: '/',
        component: Home,
        name: 'home',
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/registration',
        name: 'registration',
        component: Registration
    },
    {
        path: '/cards',
        name: 'cards',
        component: Cards
    },
    {
        path: '/tests',
        name: 'tests',
        component: Cards
    }
];