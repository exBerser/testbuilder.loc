<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/api', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
//    'namespace'  => 'Modules\API\Http\Controllers',
//    'prefix'     => 'api',
    'as'         => 'api.'
], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/login', 'Auth\LoginController@login');
//    Route::get('/check', 'Auth\LoginController@checkAuth'); //TODO: Why return blade?
        Route::post('/check', 'Auth\LoginController@checkAuth');
        Route::post('/register', 'Auth\RegisterController@register');
        Route::middleware('auth:api')->post('/logout', 'Auth\LoginController@logout');
    });
});
