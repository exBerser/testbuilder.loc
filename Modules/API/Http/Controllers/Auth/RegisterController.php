<?php

namespace Modules\API\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\User\Http\Requests\RegisterRequest;
use Modules\Users\Entities\User;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param RegisterRequest $request
     *
     * @return Response
     */
    public function register(RegisterRequest $request)
    {
        $this->_createUser($request->all());

        \Request::merge([
            'username'      => $request->get('email'),
            'password'      => $request->get('password'),
        ]);

        $token = Request::create(
            url('api/auth/login'),
            'POST'
        );

        return \Route::dispatch($token);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    private function _createUser(array $data)
    {
        $user = User::create([
            'name'       => $data['name']                   ?? null,
            'email'      => $data['email']                  ?? null,
            'password'   => Hash::make($data['password'])   ?? null,
        ]);

        return $user;
    }
}
